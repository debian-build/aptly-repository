#!/bin/bash -e

[ -d aptly/public ] || exit 0
cp -r aptly/public .

cp aptly/repo.pub public/

export TEXT=$(cat README.md)

curl -X POST \
  -H "Content-Type: application/json" \
  -d "$(jq -n '.text = (env.TEXT) | .mode = "markdown"')" \
  https://gitlab.com/api/v4/markdown | jq -r ".html" > public/index.html
