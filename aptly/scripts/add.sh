#!/bin/bash -e

gpg1 --import <(echo "$PRIVATE_KEY") 2> /dev/null

envsubst < aptly/aptly.conf > ~/.aptly.conf

for NAMESPACE in $(aptly repo list -raw)
do
    DISTRIBUTION=${NAMESPACE/%\-*/}
    DISTRIBUTION_RELEASE=${NAMESPACE##*-}
    for PACKAGE_FILE in "$@"
    do
        aptly repo add $NAMESPACE $PACKAGE_FILE
    done
    aptly publish update $DISTRIBUTION_RELEASE filesystem:git:$DISTRIBUTION
done
