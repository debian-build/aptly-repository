#!/bin/bash -e

# Download artifacts
# Needs PRIVATE_TOKEN PROJECT_ID PIPELINE_ID JOB_NAME

BASE_URL=${BASE_URL:-$(echo $CI_PROJECT_URL |  cut -d'/' -f1-3)}

FILTER=".[] | select(.status==\"success\") | select(.pipeline.id==$PIPELINE_ID) | select(.name==\"$JOB_NAME\") | .id"

JOB_ID=$(curl -s -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects/${PROJECT_ID}/jobs?per_page=${PER_PAGE:-50}" | jq -c "$FILTER")

curl -fksSL -o artifacts.zip -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects/${PROJECT_ID}/jobs/${JOB_ID}/artifacts"

unzip -q -d artifacts artifacts.zip

# Add to repo
# Needs PRIVATE_KEY, DISTRIBUTION, DISTRIBUTION_RELEASE

NAMESPACE=$DISTRIBUTION-$DISTRIBUTION_RELEASE

gpg1 --import <(echo "$PRIVATE_KEY") 2> /dev/null

envsubst < aptly/aptly.conf > ~/.aptly.conf

aptly repo show $NAMESPACE 2> /dev/null || aptly repo create $NAMESPACE

for PACKAGE_FILE in $(find artifacts -type f -name '*.deb')
do
    aptly repo add $NAMESPACE $PACKAGE_FILE
done

aptly publish repo -distribution=$DISTRIBUTION_RELEASE $NAMESPACE filesystem:git:$DISTRIBUTION || aptly publish update $DISTRIBUTION_RELEASE filesystem:git:$DISTRIBUTION
