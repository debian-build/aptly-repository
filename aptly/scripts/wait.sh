#!/bin/bash -e

printf 'Waiting.'

BASE_URL=${BASE_URL:-$(echo $CI_PROJECT_URL |  cut -d / -f1-3)}

while true; do
    READY=$(curl -sS --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects/${CI_PROJECT_ID}/pipelines?order_by=id&sort=asc&scope=running" | jq '.[0].id=='"${CI_PIPELINE_ID}")
    if [ "${READY}" = "true" ]; then
        printf ' Ready!\n'
        exit 0
    else
        printf '.'
        sleep 10
    fi
done
