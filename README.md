# Aptly repository
[![pipeline status](https://gitlab.com/debian-build/aptly-repository/badges/master/pipeline.svg)](https://gitlab.com/debian-build/aptly-repository/commits/master)

## Setup on Debian 9 (stretch)

```bash
wget -O- http://deb.ztier.link/repo.pub | sudo apt-key add -
echo deb http://deb.ztier.link/debian stretch main | sudo tee /etc/apt/sources.list.d/ztier.list
sudo apt-get update
```

## Setup on Debian 10 (buster)

```bash
wget -O- http://deb.ztier.link/repo.pub | sudo apt-key add -
echo deb http://deb.ztier.link/debian buster main | sudo tee /etc/apt/sources.list.d/ztier.list
sudo apt-get update
```

## Setup on Ubuntu 18.04 LTS (bionic)

```bash
wget -O- http://deb.ztier.link/repo.pub | sudo apt-key add -
echo deb http://deb.ztier.link/ubuntu bionic main | sudo tee /etc/apt/sources.list.d/ztier.list
sudo apt-get update
```
